from flask import Flask, render_template, request, redirect, url_for, jsonify, json
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		{'title': 'Algorithm Design', 'id':'2'}, \
		{'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return r

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('home.html',books=books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
    if request.method == 'POST':
        book = request.form['book_name']
        if book != "":
            books.append({'title': book, 'id': get_uid()})
            return redirect('/book/')
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        if request.form['bookName'] != '':
            for i in range(len(books)):
                if str(books[i]['id']) == str(book_id):
                    books[i]['title'] = request.form['bookName']
                    return redirect(url_for('showBook'))
        
    for i in books:
        if str(i['id']) == str(book_id):
            title = i['title']
            return render_template('editBook.html', book=i)
    return render_template('error.html')
    
    # <your code>
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for i in range(len(books)):
                if str(books[i]['id']) == str(book_id):
                    del books[i]
                    return redirect(url_for('showBook'))
    for i in books:
        if str(i['id']) == str(book_id):
            title = i['title']
            return render_template('deleteBook.html', book=i)

    # <your code>

def get_uid():
    seen = []
    start = 1
    for i in books:
        seen.append(i['id'])
    while str(start) in seen:
        start += 1
    return str(start)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)

